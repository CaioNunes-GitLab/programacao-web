package br.ucsal.wed.tarefa;

public class TarefaService {
	
	TarefaDAO dao = new TarefaDAO();

	public void inserir(Tarefa tarefa) {
		tarefa.setConcluida(false);
		dao.inserir(tarefa);
	}
	
}
