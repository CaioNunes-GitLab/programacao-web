package br.ucsal.wed.tarefa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TarefaDAO {
	
	
	private static final Map<Integer, Tarefa> tabela = new HashMap();
	private static AtomicInteger pk = new AtomicInteger();
	
	public TarefaDAO() {
		
	}
	
	public List<Tarefa> listar(){
		return List.copyOf(tabela.values());
	}
	
	public void excluir(Integer id) {
		Tarefa entity = tabela.remove(id);
	}
	
	public Tarefa buscar(Integer id) {
		return tabela.get(id);
	}


	public void inserir(Tarefa tarefa) {
		Integer id = pk.getAndIncrement();
		tarefa.setId(id);
		tabela.put(id, tarefa);
	}
	
}
