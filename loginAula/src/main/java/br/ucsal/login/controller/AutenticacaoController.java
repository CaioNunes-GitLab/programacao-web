package br.ucsal.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.security.auth.message.callback.PrivateKeyCallback.Request;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/autenticar")
public class AutenticacaoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();

		//A nível de Page
		String usuario = req.getParameter("usuario");
		String senha = req.getParameter("senha");
		System.out.println(usuario);
		out.write(usuario + "");

		if (usuario != null) {
			if (usuario.equalsIgnoreCase(senha)) {
				// Redirecionamento passando os parâmetros na URL
				// resp.sendRedirect("./sistema.jsp?usuario="+usuario);
				// forward
				req.getRequestDispatcher("sistema.jsp").forward(req, resp);
			} else {
				resp.sendRedirect("./login.jsp");
			}
		}
	}
}
