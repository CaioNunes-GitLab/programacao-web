<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
	String pageVar = request.getParameter("x");
	out.write("Page    -    " + pageVar);
	
	int reqVar = 0;
	int requestVar = Integer.parseInt(request.getParameter("x"));
	
	if(request.getAttribute("var_r") != null){
		Integer i = (Integer) request.getAttribute("var_r");
		i++;
		request.setAttribute("var_r", new Integer(i));
	}else{
		request.setAttribute("var_r", new Integer(requestVar));
	}
	out.write(" Request: " + reqVar);
	
	int sesVar = 0;
	if(session.getAttribute("sesVar") != null){
		Integer i = (Integer) session.getAttribute("sesVar");
		sesVar = i;
		i++;
		session.setAttribute("sesVar", new Integer(i));
	}else{
		session.setAttribute("sesVar", new Integer(sesVar));
	}
	out.write(" Session: " + sesVar);
	
	int appVar = 0;
	if(application.getAttribute("appVar") != null){
		Integer i = (Integer) application.getAttribute("appVar");
		appVar = i;
		i++;
		application.setAttribute("appVar", new Integer(i));
	}else{
		application.setAttribute("appVar", new Integer(appVar));
	}
	out.write(" Application: " + appVar);
	%>
</body>
</html>